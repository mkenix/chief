#pragma once
#include <SFML/Graphics.hpp>
#include <map.h>
#include <fstream>
#include <string>

class Entity
{
public:
	float w, h, dx, dy, speed, x, y, moveTimer, CurrentFrame; //���������� ������ � � �, ������ ������, ��������� (�� � � �� �), ���� �������� 
	int health, color;//����������� (direction) �������� ������, ����, ��
	bool life, isMove, isSelect, onGround;	//������ �����, ���������� ��������� �������� � ������ �������
	sf::Texture texture;//���� ��������
	sf::Sprite sprite;//���� ������
	sf::String name;//����� ����� ���� �������, ����� ������ ������ ����� � ��������� � �����
	Entity(sf::Image& image, float X, float Y, int W, int H, sf::String Name)
	{
		x = X; y = Y; w = W; h = H; name = Name; moveTimer = 0; CurrentFrame = 0;
		dx = 0;dy = 0;speed = 0; health = 100; color = 0;
		life = true; isMove = false; isSelect = false; onGround = false;	//�������������� �����
		texture.loadFromImage(image);
		sprite.setTexture(texture);
		sprite.setOrigin(w / 2, h / 2);
	}
};
////////////////////////////////////////////////////����� ������////////////////////////
class Player :public Entity // ����� ������
{
public:
	enum stateObject { left, right, up, down} state;	//��������� ��� ������������ - ��������� �������
	int playerScore;	//���� ������, ���������� ����������
	

	Player(sf::Image(image), float X, float Y, float W, float H, sf::String Name) :Entity(image, X, Y, W, H, Name)
	{
		playerScore = 0; state = down;	//�������������� ����������
		if (name == "Player1")
		{
			sprite.setTextureRect(sf::IntRect(0, 0, w, h)); //������ ������� ���� ������������� ��� ������ ������ ����, � �� ���� ����� �����. sf::IntRect - ���������� �����
		}
	}

	void control()
	{
		if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Left))) {
			state = left; //state = left - ����������� �����, speed =0.1 - �������� ��������. �������� - ����� �� ��� ����� �� �� ��� �� �������� � ����� �� ���������� ������ ���
			speed = 0.1;
			/*CurrentFrame += 0.005 * time;
			if (CurrentFrame > 3) CurrentFrame -= 3;
			p.sprite.setTextureRect(sf::IntRect(57 * int(CurrentFrame) + 57, 108, -57, 108)); */ //����� ������ p ������ player ������ ������, ����� �������� (��������� �������� �����)
		}

		if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Right))) {
			state = right; //����������� ������, �� ����
			speed = 0.1;
			/*CurrentFrame += 0.005 * time;
			if (CurrentFrame > 3) CurrentFrame -= 3;
			p.sprite.setTextureRect(sf::IntRect(57 * int(CurrentFrame), 108, 57, 108));*/ //����� ������ p ������ player ������ ������, ����� �������� (��������� �������� �����)
		}

		if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Up))) {
			state = down; //����������� ����, �� ����
			speed = 0.1;
			/*CurrentFrame += 0.005 * time;
			if (CurrentFrame > 3) CurrentFrame -= 3;
			p.sprite.setTextureRect(sf::IntRect(57 * int(CurrentFrame), 217, 57, 108));*/ //����� ������ p ������ player ������ ������, ����� �������� (��������� �������� �����)
		}

		/*if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) && (onGround)) {
			state = jump; dy = -0.4; onGround = false; //�.�. ��������� ����� ������, �������� � ��������, ��� ������ ��������

		}*/

		if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Down))) { //���� ������ ������� ������� ����� ��� ���� ����� �
			state = up;  //����������� �����, �� ����
			speed = 0.1;
			/*CurrentFrame += 0.005 * time; //������ ��� ����������� �� "������". ���������� ������� �� ���� �������� ������������ ������� � ��������. ������� 0.005 ����� �������� �������� ��������
			if (CurrentFrame > 3) CurrentFrame -= 3; //���������� �� ������ � ������� �� ������ ������������. ���� ������ � �������� ����� - ������������ �����.
			p.sprite.setTextureRect(sf::IntRect(57 * int(CurrentFrame), 0, 57, 108));*/ //���������� �� ����������� �. ���������� 96,96*2,96*3 � ����� 96
		}
	}

	void update(float time) //������� "���������" ������� ������. update - ����������. ��������� � ���� ����� SFML , ���������� ���� �������� ����������, ����� ��������� ��������.
	{
		
		if (life)
		{
			control();
			switch (state)//��������� ��������� � ����������� �� �����������. (������ ����� ������������� �����������)
			{
			case right:
				dx = speed; dy = 0;
				CurrentFrame += 0.005 * time;
				if (speed!=0)
				{
					if (CurrentFrame > 3) CurrentFrame -= 3;
					sprite.setTextureRect(sf::IntRect(57 * int(CurrentFrame), 108, 57, 108));
				}
				break;//�� ���� ������ ������������� ��������, �� ������ ��������. ��������, ��� �������� ���� ������ ������
			case left:
				dx = -speed; dy = 0;
				CurrentFrame += 0.005 * time;
				if (speed!=0)
				{
					if (CurrentFrame > 3) CurrentFrame -= 3;
					sprite.setTextureRect(sf::IntRect(57 * int(CurrentFrame) + 57, 108, -57, 108));
				}
				break;//�� ���� ������ ������������� ��������, �� ������ ��������. ����������, ��� �������� ���� ������ �����
			case down:
				dx = 0; dy = -speed;
				CurrentFrame += 0.005 * time;
				if (speed!=0)
				{
					if (CurrentFrame > 3) CurrentFrame -= 3;
					sprite.setTextureRect(sf::IntRect(57 * int(CurrentFrame), 217, 57, 108));
				}
				break;//�� ���� ������ ������� ��������, �� ������ �������������. ����������, ��� �������� ���� ������ ����
			case up:
				dx = 0; dy = speed;
				CurrentFrame += 0.005 * time;
				if (speed!=0)
				{
					if (CurrentFrame > 3) CurrentFrame -= 3;
					sprite.setTextureRect(sf::IntRect(57 * int(CurrentFrame), 0, 57, 108));
				}
				break;//�� ���� ������ ������� ��������, �� ������ �������������. ����������, ��� �������� ���� ������ �����
			}
			x += dx * time;
			checkCollisionWithMap(dx, 0);//������������ ������������ �� �
			y += dy * time;
			checkCollisionWithMap(0, dy);//������������ ������������ �� Y
		}
		else
		{
			speed=0;
			if (CurrentFrame < 3) { CurrentFrame = 3; }
			CurrentFrame += 0.005 * time;
			if (CurrentFrame > 7) CurrentFrame = 6;
			switch (state)
			{
			case right:
				sprite.setTextureRect(sf::IntRect(57 * int(CurrentFrame), 108, 57, 108)); break;
			case left:
				sprite.setTextureRect(sf::IntRect(57 * int(CurrentFrame) + 57, 108, -57, 108)); break;
			case up:
				sprite.setTextureRect(sf::IntRect(57 * int(CurrentFrame), 0, 57, 108)); break;
			case down:
				sprite.setTextureRect(sf::IntRect(57 * int(CurrentFrame), 217, 57, 108)); break;
			default:
				break;
			}
		}
		
		
		sprite.setPosition(x + w / 2, y + h / 2); //������ ������� ������� � ����� ��� ������
		if (health <= 0) { life = false; }
		if (!isMove) { speed = 0; }
		//if (!onGround) { dy = dy + 0.0015*time; }//������� � ����� ������ ����������� � �����
		if (life) { setPlayerCoordinateForView(x, y); }
	}

	void checkCollisionWithMap(float Dx, float Dy)//� ��� �������� ������������ � ������
	{
		for (int i = y / 32; i < (y + h) / 32; i++)//���������� �� ��������� �����
			for (int j = x / 32; j < (x + w) / 32; j++)
			{
				if (TileMap[i][j] == '0')//���� ������� ��� ������ �����? ��
				{
					if (Dy > 0) { y = i * 32 - h;  dy = 0; onGround = true; }//�� Y ����=>���� � ���(����� �� �����) ��� ������. � ���� ������ ���� ���������� ��������� � ��������� ��� �� �����, ��� ���� ������� ��� �� �� ����� ��� ����� ����� ����� �������
					if (Dy < 0) { y = i * 32 + 32;  dy = 0; }//������������ � �������� ������ �����(����� � �� �����������)
					if (Dx > 0) { x = j * 32 - w; }//� ������ ����� �����
					if (Dx < 0) { x = j * 32 + 32; }// � ����� ����� �����
				}
				//else { onGround = false; }//���� ������ �.� �� ����� ���������� � �� ������ ����������� ��� ��������� ������� �������� ����

				if (TileMap[i][j] == 'f')	//���� ������ ����� 'f' (�����)
				{
					health -= 40;	//���� ����� �����, �������� ��, ���� ��������� health = health-40
					TileMap[i][j] = ' ';	//������� �����
					color = -1;
				}

				if (TileMap[i][j] == 'h')	//���� ������ ����� 'h' (�����)
				{
					health += 20;	//���� ����� �����, health = health+20
					TileMap[i][j] = ' ';	//������� �����
					color = 1;
				}
			}
	}


	std::string death_words()
	{
		std::ifstream file("bases/base.txt");
		int numLines = std::count(std::istreambuf_iterator<char>(file),
			std::istreambuf_iterator<char>(), '\n');
		file.seekg(0);
		char open_file[400];
		int index = -1, a = rand() % numLines, count = 0;
		do
		{
			index++;
			file.getline(open_file, 400);
		} while (index < a);
		file.close();
		std::string death_word = open_file;
		return death_word;
	}

	std::string split_string(const std::string& s)
	{
		if (!s.size()) {
			return "";
		}
		std::stringstream ss;
		ss << s[0];
		bool flag = false;
		for (int i = 1; i < s.size(); i++) 
		{		
			if (i % 25 == 0) { ss << s[i]; flag = true; }
			else { ss << s[i]; }
			if (flag)
			{
				if (s[i + 1] == ' ') { ss << '\n'; flag = false; }
			}
		}
		return ss.str();
	}
	
};

