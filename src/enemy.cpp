#include "enemy.hpp"

namespace mk
{
	void move_player(sf::Sprite player, float time)
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			player.move(-0.5 * time, 0);
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		{
			player.move(0.5 * time, 0);
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			player.move(0, -0.5 * time);
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		{
			player.move(0, 0.5 * time);
		}
	}
}