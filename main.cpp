#include <SFML/Graphics.hpp>
#include <sstream>
#include <iostream>
#include <enemy.hpp>
#include <view.h>
#include <mission.h>
#include <player.hpp>

//#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")		//����� �������� ��������� ������ �� ����

int main()
{
	randomMapGenerate();

	sf::RenderWindow window(sf::VideoMode(640, 480), "Game");
	view.reset(sf::FloatRect(0, 0, 640, 480));

	sf::Font font;	//�����
	if (!font.loadFromFile("fonts/CyrilicOld.ttf"))
	{
		std::cout << "ERROR : font was not loaded!" << std::endl;
			return -1;
	}
	

	
	//text.setStyle(sf::Text::Bold);	//������ �����
	

	sf::Text mission_text("", font, 20);	//������ ������ �����. ���������� � ���� ������, �����, ������ ������(� ��������);//��� ������ �����(�� ������)
	mission_text.setOutlineColor(sf::Color::Black);	//��������� ����� � �������. �� ��������� �� �����
	mission_text.setFillColor(sf::Color::Black);

	sf::Image map_image;
	if (!map_image.loadFromFile("img/map.png"))
	{
		std::cout << "ERROR: image was not loaded!" << std::endl;
		return -1;
	}
	sf::Texture map;
	map.loadFromImage(map_image);
	sf::Sprite map_sprite;
	map_sprite.setTexture(map);


	sf::Image quest_image;
	if (!quest_image.loadFromFile("img/missionbg.jpg"))
	{
		std::cout << "ERROR: quest_image was not loaded!" << std::endl;
		return -1;
	}
	//quest_image.createMaskFromColor(sf::Color(0, 0, 0));
	sf::Texture quest_texture;
	quest_texture.loadFromImage(quest_image);
	sf::Sprite quest_sprite;
	quest_sprite.setTexture(quest_texture);
	quest_sprite.setTextureRect(sf::IntRect(0, 0, 340, 510));	//���������� �����, ������� �������� ��������
	quest_sprite.setScale(0.6f, 0.6f);							//�������� �������� -> ������ ���� ������

	sf::Image heroImage;
	heroImage.loadFromFile("img/enemy.png");

	Player p(heroImage, 250, 500, 57, 108, "Player1");

	sf::Text death_text("���, �� ���������!\n\n" + p.split_string(p.death_words()), font, 25);	//������ ������ �����. ���������� � ���� ������, �����, ������ ������(� ��������);//��� ������ �����(�� ������)
	death_text.setOutlineColor(sf::Color::Red);	//��������� ����� � �������. �� ��������� �� �����
	death_text.setFillColor(sf::Color::Red);
	//������������ ������ ����� � ������ ������, ����� ����������� ���������
	
	sf::CircleShape circle(10.f);

	bool showMissionText = true;	//���������� ����������, ���������� �� ��������� ������ ������ �� ������

	//float CurrentFrame = 0;
	sf::Clock clock;
	bool isMove = false;			//���������� ��� ������ ���� �� �������
	float dX = 0;					//������������� �������� �� x
	float dY = 0;					//�� y
	int tempX = 0;					//��������� ����� �.������� �� ����� ������� ���� ���� ����
	int tempY = 0;					//����� Y
	float distance = 0;				//��� ���������� �� ������� �� ���� �������
	float timer = 0;				//������ �����������

	/*sf::Clock gameTimeClock;	//���������� �������� �������
	int gameTime = 0;			//�������� ������� �����, ����������������*/

	while (window.isOpen())
	{
		float time = clock.getElapsedTime().asMicroseconds();
		
		/*if (p.life) gameTime = gameTimeClock.getElapsedTime().asSeconds();
		//������� ����� ������ ��� �����, ���� �� ���, ������������� ��� time ��� �� ����. ��� �� ����������� */
 
		clock.restart();
		time = time / 800;

		sf::Vector2i pixelPos = sf::Mouse::getPosition(window);	// �������� ���������� �������
		sf::Vector2f pos = window.mapPixelToCoords(pixelPos);	//��������� �� � ������� (������ �� ��������� ����)
		//std::cout << pixelPos.x << std::endl;		//������� �� ���������� x ������� ������� � ������� (��� �� ����� ������ ������ ����)
		//std::cout << pos.x << std::endl;			//������� �� X, ������� ��������������� � ������� ����������

		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
			
			if(event.type == sf::Event::MouseButtonPressed)	//���� ������ ������� ����
				if(event.key.code == sf::Mouse::Left)		//� ������ �����
					if (p.sprite.getGlobalBounds().contains(pos.x, pos.y))	//� ��� ���� ���������� ������� �������� � ������
					{
						std::cout << "isClicked!\n";	//������� �� ������� ��������� �� ����
						p.sprite.setColor(sf::Color::Green);	//���������� � �������
						p.isSelect = true;	//���������� ������ ���������� true
					}
			if(p.isSelect)	//���� ������� ������
			if (event.type == sf::Event::MouseButtonPressed)	//���� ������ �������
			if (event.key.code == sf::Mouse::Right)			//� ������ ������
			{
				p.isMove = true;		//�� �������� ��������
				p.isSelect = false;		//������ ��� �� ������
				p.sprite.setColor(sf::Color::White);		//���������� ������� ���� �������
				tempX = pos.x;	//���������� ���������� ������� �� ����� ������� �� ������� �� X
				tempY = pos.y;	//� �� Y
				/*											//������� ������� �� ������ ������ ������ ����
				float dX = pos.x - p.x;	//������, ������������ ������, ������� ���������� ������ � ������
				float dY = pos.y - p.y;	//�� �� �� Y
				float rotation = (atan2(dY, dX)) * 180 / 3.14159265;	//�������� ���� � �������� � ��������� ��� � �������
				//std::cout << rotation << std::endl;		//������� �� ������� � �������
				p.sprite.setRotation(rotation);
				*/
			}
				
						
			
			if(event.type == sf::Event::KeyPressed)		//������� ������� �������
			if ((event.key.code == sf::Keyboard::Tab))
			{
				switch (showMissionText)	//�������������, ����������� �� ���������� ���������� � ������� ������
				{
				case true:
				{
					std::ostringstream playerHealthString;	//������ ��������
					playerHealthString << p.health; 		//������� �������� �������� � ������, ��������� ������.
					std::ostringstream task;				//������ ������ ������
					task << getTextMission(getCurrentMission(p.x));
						//���������� ������� getTextMission(��� ���������� ����� ������),
						//������� ��������� � �������� ��������� ������� getCurrentMission(������������ ����� ������),
						//� ��� ��� �-��� ��������� � �������� ��������� ������� p.getplayercoordinateX() (��� �-��� ���������� ��� ���������� ������
					mission_text.setString("��������: " + playerHealthString.str() + "\n" + task.str());	//����� ������
					mission_text.setPosition(view.getCenter().x + 125, view.getCenter().y - 130);				//������� ���� ��� �����
					quest_sprite.setPosition(view.getCenter().x + 115, view.getCenter().y - 130);		//������� ���� "������" ��� ���������� �����
					showMissionText = false;
					break;
				}
				case false:
				{
					mission_text.setString("");		//���� �� ����� Tab, �� ���� ����� ������
					showMissionText = true;	//��� ������ ��������� ����� ������ Tab � ������� ������ �� �����
					break;
				}
				}
			}
			
		}

		if (p.isMove)		//���� ����� ������� ������
		{
			distance = sqrt((tempX - p.x) * (tempX - p.x) + (tempY - p.y) * (tempY - p.y));		//������� ���������(���������� �� �����������)
			if (p.life)		//���� ����� ���
			{
				if (distance > 2)	//���� �������� ������� �������� �� ����� �������� ������� �������
				{
					p.x += 0.1 * time * (tempX - p.x) / distance;	//�������� �� X � ������� ������� �������
					p.y += 0.1 * time * (tempY - p.y) / distance;	//�������� �� Y � ������� ������� �������
				}
				else { p.isMove = false; /*std::cout << "Movevent is over" << std::endl;*/ }	//�������, ��� �������� ��������
			}
			else { p.isMove = false; /*std::cout << "Movevent is over" << std::endl;*/ }
			
		}

		///////////////////////////////////////////���������� ���������� � ���������////////////////////////////////////////////////////////////////////////
		
		setPlayerCoordinateForView(p.x, p.y);	//������� ���������� ������ � ������� ���������� �������

		/*													!!!��������� ����� ��������!!!
		sf::Vector2i localPosition = sf::Mouse::getPosition(window);	//������� � ������ ���������� ���� ������������ ���� (x,y)
		std::cout << "locPos " << localPosition.x << std::endl;			//�������, ��� ���� ���� ������� ������� X
		if (localPosition.x < 10) { view.move(-0.2 * time, 0); }			//���� ������ �������� � ����� ���� ������, ������� ������ �����
		if (localPosition.x > window.getSize().x - 10) { view.move(0.2 * time, 0); }	//������ ���� - ������
		if (localPosition.y > window.getSize().x - 10) { view.move(0, 0.2 * time); }	//������ ���� - ����
		if (localPosition.y < 10) { view.move(0, -0.2 * time); }			//������� ���� - �����
		*/

		p.update(time);		/*�������� ������ p ������ Player � ������� ������� sfml, ��������� �����
							� �������� ��������� ������� update. ��������� ����� �������� ����� ���������*/
		//viewMap(time);		//�-�� ���������� ������, ������� �� ����� sfml
		//changeView();
		window.setView(view);	//"��������" ������ � ���� sfml
		window.clear(sf::Color(128,106,89));

		/////////////////////////////////////��������� �����/////////////////////////////////////
		for(int i = 0; i < HEIGHT_MAP; i++)
		for (int j = 0; j < WIDTH_MAP; j++)
		{
			if (TileMap[i][j] == ' ') map_sprite.setTextureRect(sf::IntRect(0, 0, 32, 32));		//���
			if (TileMap[i][j] == 's') map_sprite.setTextureRect(sf::IntRect(32, 0, 32, 32));	//������
			if (TileMap[i][j] == '0') map_sprite.setTextureRect(sf::IntRect(64, 0, 32, 32));	//�����
			if (TileMap[i][j] == 'f') map_sprite.setTextureRect(sf::IntRect(96, 0, 32, 32));	//��
			if (TileMap[i][j] == 'h') map_sprite.setTextureRect(sf::IntRect(128, 0, 32, 32));	//���

			map_sprite.setPosition(j * 32, i * 32);

			window.draw(map_sprite);
		}
		/* std::ostringstream playerScoreString;	//�������� ���������� �����
		playerScoreString << p.playerScore;
		text.setString("������� ������:" + playerScoreString.str());	//����� ������ ������
		text.setPosition(view.getCenter().x - 165, view.getCenter().y - 200);	//����� ������� ������ - ����� ������
		window.draw(text);	//������ ���� ����� */

		if (!showMissionText)
		{
			mission_text.setPosition(view.getCenter().x + 125, view.getCenter().y - 130);			//������� ����� ���������� �����
			quest_sprite.setPosition(view.getCenter().x + 115, view.getCenter().y - 130);	//������� ���� ��� �����
			window.draw(quest_sprite); window.draw(mission_text);//������ ������ ������, ����� � �����. �� �������� �� ���������� ����������, ���������� �� Tab
		}	
		

		if (p.color != 0)		//���� ���� �� �����, �� ������������ ���
		{
			(p.color == 1 ? p.sprite.setColor(sf::Color::Green) : p.sprite.setColor(sf::Color::Red));
			timer += 0.1 * time;
			if (timer >= 20)
			{
				p.color = 0;
				p.sprite.setColor(sf::Color::White);
				timer = 0;
			}
			
		}
		else
		{
			(p.isMove || p.isSelect ? p.sprite.setColor(sf::Color::Green) : p.sprite.setColor(sf::Color::White));
		}

		window.draw(p.sprite);//������ ������ ������� p ������ player
		 
		if (!p.life) { death_text.setPosition(view.getCenter().x - 160 , view.getCenter().y - 130 ); window.draw(death_text); }//������ ���������� �����
		
		window.display();
	}

	return 0;
}
